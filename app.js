// Setup
var express = require('express');
var app = express();
var mongoose = require('mongoose')
mongoose.connect("mongodb://localhost:27017/node-blog")
var bodyParser = require('body-parser')
var arp = require('node-arp');
var ping = require('ping');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true}))
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

var MacAddressSchema = new mongoose.Schema({ MAC: String,name: String, owner:String, type:String});
var postSchema = new mongoose.Schema({ body: String,parent: String});
var MACAddresses = new mongoose.model('MACAddresses', MacAddressSchema);
var Post = new mongoose.model('Post', postSchema);
// Routes

var IPAdresses = []

function getIPAdresses(ip)
{
    if (ip < 256)
    {
        ping.promise.probe('192.168.0.' + ip)
    .then(function (res) {
        if (res.alive){
        arp.getMAC('192.168.0.'+ip, function(err, mac) {
            if (!err) {
                if (IPAdresses.find((e) => e.MAC === mac) === undefined)
                IPAdresses.push({ip:'192.168.0.'+ip,MAC:mac});
                else
                IPAdresses.find((e) => e.MAC === mac).ip = '192.168.0.'+ip;
                MACAddresses.updateOne({MAC:mac},{MAC:mac},{upsert:true}, (err,doc) =>{
                });
            }
        });}
        getIPAdresses(ip+1);
    });
    }
    else
    {
        setTimeout(getIPAdresses(1),10000);
    }
}

getIPAdresses(1);


app.post('/addpost', (req, res) => {
    var postData = new Post(req.body);
    postData.save().then( result => {
        res.redirect('/');
    }).catch(err => {
        res.status(400).send(err);
    });
});

app.post('/changeMac', (req, res) => {
    MACAddresses.updateOne({MAC:req.body.MAC},req.body,{},(err,doc)=> {}).then( result => {
        res.redirect('/');
    }).catch(err => {
        res.status(400).send(err);
    });
});

app.get("/", (req, res) => {

    MACAddresses.find({}, (err, macs) => {
        macs.forEach(element => {

            FoundMac = IPAdresses.find((e) => element.MAC === e.MAC);
            if (FoundMac != undefined)
            if (element.MAC === FoundMac.MAC) element.ip = FoundMac.ip;
        });
    res.render('index', { macs: macs})
   });
});

app.get("/editMac", (req, res) => {
    MACAddresses.findOne({MAC:req.query.mac}, (err, macs) => {
        if (macs === null)
            res.send('Nie udało się znaleźć Mac adresu <input type="button" value="Anuluj" onclick="window.location.replace(\'/\')"/>');
        else
            res.render('editMac',{entry:macs});
   });
});

// Listen
app.listen(80, () => {
    console.log('Server listing on 80');
})